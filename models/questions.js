const userModule =  require("./user");

const questionsModule = {
    currentQuestion: null,
    currentTestIndex: 0,

    testMode: false,
    tests: [],

    questions: [
        [1, "water", "آب"],
        [2, "summer", "تابستان"],
        [3, "sleep", "خوابیدن"],
        [4, "enough", "کافی"],
        [5, "cold", "سرد"],
        [6, "bag", "کیف"],
        [7, "green", "سبز"],
        [8, "book", "کتاب"],
        [9, "door", "در"],
        [10, "abandon", "ترک کردن"],
        [11, "flower", "گل"],
        [12, "soap", "صابون"],
        [13, "key", "کلید"],
        [14, "red", "قرمز"],
        [15, "face", "چهره"],
        [16, "car", "ماشین"],
        [17, "stone", "سنگ"],
        [18, "sea", "دریا"],
        [19, "wallet", "کیف پول"],
        [20, "future", "آینده"],
        [21, "friend", "دوست"],
        [22, "shape", "شکل"],
        [23, "circle", "دایره"],
        [24, "hospital", "بیمارستان"],
        [25, "tree", "درخت"],
        [26, "clock", "ساعت"],
        [27, "blood", "خون"],
        [28, "body", "بدن"],
        [29, "man", "مرد"],
        [30, "woman", "زن"],
        [31, "baby", "بچه"],
        [32, "eat", "خوردن"],
        [33, "angry", "عصبانی"],
        [34, "mosque", "مسجد"],
        [35, "church", "کلیسا"],
        [36, "city", "شهر"],
        [37, "mayor", "شهردار"],
        [38, "season", "فصل"],
        [39, "tape", "نوار"],
        [40, "miner", "معدنچی"],
        [41, "pen", "خودکار"],
        [42, "history", "تاریخ"],
        [43, "school", "مدرسه"],
        [44, "dinner", "شام"],
        [45, "lunch", "نهار"],
        [46, "war", "جنگ"],
        [47, "famous", "مشهور"],
        [48, "king", "پادشاه"],
        [49, "image", "تصویر"],
        [50, "sun", "خورشید"],
        [51, "sky", "آسمان"],
        [52, "star", "ستاره"],
        [53, "house", "خانه"],
        [54, "shark", "کوسه"],
        [55, "economy", "اقتصاد"],
        [56, "kind", "مهربان"],
        [57, "coin", "سکه"],
        [58, "holy", "مقدس"],
        [59, "peach", "هلو"],
        [60, "math", "ریاضی"],
        [61, "carpet", "فرش"],
        [62, "cow", "گاو"],
        [63, "hello", "سلام"],
        [64, "go", "رفتن"],
        [65, "see", "دیدن"],
        [66, "gun", "تفنگ"],
        [67, "far", "دور"],
        [68, "almost", "تقریبا"],
        [69, "cash", "نقد"],
        [70, "money", "پول"],
        [71, "hold", "نگه داشتن"],
        [72, "great", "بزرگ"],
        [73, "normal", "عادی"],
        [74, "rescue", "نجات دادن"],
        [75, "fall", "افتادن"],
        [76, "steel", "فولاد"],
        [77, "wheel", "چرخ"],
        [78, "dog", "سگ"],
        [79, "search", "جستجو کردن"],
        [80, "void", "خلاء"],
        [81, "tiger", "ببر"],
        [82, "window", "پنجره"],
        [83, "glasses", "عینک"],
        [84, "nose", "بینی"],
        [85, "have", "داشتن"],
        [86, "office", "اداره"]
        [87, "university", "دانشگاه"],
        [88, "space", "فضا"],
        [89, "hat", "کلاه"],
        [90, "skirt", "دامن"],
        [91, "gray", "خاکستری"],
        [92, "white", "سفید"],
        [93, "people", "مردم"],
        [94, "army", "ارتش"],
        [95, "satelite", "ماهواره"],
        [96, "artist", "هنرمند"],
        [97, "peace", "صلح"],
        [98, "science", "علم"],
        [99, "movie", "فیلم"],
        [100, "egg", "تخم مرغ"],
        [101, "corn", "ذرت"],
        [102, "breakfast", "صبحانه"],
        [103, "page", "صفحه"],
        [104, "next", "بعدی"],
        [105, "yard", "حیاط"],
        [106, "camera", "دوربین"],
        [107, "head", "سر"],
        [108, "eye", "چشم"],
        [109, "lip", "لب"],
        [110, "foot", "پا"],
        [111, "earth", "زمین"],
        [112, "air", "هوا"],
        [113, "metal", "فلز"],
        [114, "map", "نقشه"],
        [115, "pattern", "الگو"],
        [116, "friday", "جمعه"],
        [117, "lung", "ریه"],
        [118, "heavy", "سنگین"],
        [119, "deep", "عمیق"],
        [120, "tight", "تنگ"],
        [121, "expensive", "گران"],
        [122, "deaf", "کر"],
        [123, "sick", "مریض"],
        [124, "wet", "خیس"],
        [125, "pray", "دعا کردن"],
        [126, "wash", "شستن"],
        [127, "close", "بستن"],
        [128, "teach", "درس دادن"],
        [129, "feed", "غذا دادن"],
        [130, "count", "شمردن"],
        [131, "build", "ساختن"],
        [132, "write", "نوشتن"],
        [133, "pull", "کشیدن"],
        [134, "shake", "لرزاندن"],
        [135, "dig", "حفر کردن"],
        [136, "laugh", "خندیدن"],
        [137, "night", "شب"],
        [138, "year", "سال"],
        [139, "minute", "دقیقه"],
        [140, "winter", "زمستان"],
        [141, "west", "غرب"],
        [142, "east", "شرق"],
        [143, "path", "مسیر"],
        [144, "north", "شمال"],
        [145, "south", "جنوب"],
        [146, "hole", "سوراخ"],
        [147, "bear", "خرس"],
        [148, "pink", "صورتی"],
        [149, "square", "مربع"],
        [150, "god", "خدا"],
        [151, "silver", "نقره"],
        [152, "gold", "طلا"],
        [153, "wood", "چوب"],
        [154, "tall", "بلند"],
        [155, "hot", "گرم"],
        [156, "sand", "شن"],
        [157, "wave", "موج"],
        [158, "sound", "صدا"],
        [159, "soil", "خاک"],
        [160, "forrest", "جنگل"],
        [161, "skin", "پوست"],
        [162, "knife", "چاقو"],
        [163, "cancer", "سرطان"],
        [164, "nurse", "پرستار"],
        [165, "heart", "قلب"],
        [166, "tool", "ابزار"],
        [167, "ring", "حلقه"],
        [168, "needle", "سوزن"],
        [169, "paper", "کاغذ"],
        [170, "paint", "نقاشی"],
        [171, "poem", "شعر"],
        [172, "kitchen", "آشپزخانه"],
        [173, "table", "میز"],
        [174, "fork", "چنگال"],
        [175, "apple", "سیب"],
        [176, "food", "غذا"],
        [177, "ball", "توپ"],
        [178, "drug", "دارو"],
        [179, "game", "بازی"],
        [180, "boat", "قایق"],
        [181, "prison", "زندان"],
        [182, "death", "مرگ"],
        [183, "soldier", "سرباز"],
        [184, "secretary", "منشی"],
        [185, "lawyer", "وکیل"],
        [186, "neighbor", "همسایه"],
        [187, "queen", "ملکه"],
        [188, "black", "مشکی"],
        [189, "coffee", "قهوه"],
        [190, "village", "روستا"],
        [191, "farm", "مزرعه"],
        [192, "road", "جاده"],
        [193, "bridge", "پل"],
        [194, "train", "قطار"],
        [195, "bat", "خفاش"],
        [196, "axe", "تبر"],
        [197, "wing", "بال"],
        [198, "live", "زندگی کردن"],
        [199, "dream", "رویا"],
        [200, "furnace", "کوره"],
    ],

    makeNewWord(userId) {
        const user = userModule.getUser(userId)

        if (user) {
            if (this.questions.length > user.training.seen.length + user.training.fixed.length) {
                this.currentQuestion = this.questions[user.training.seen.length + user.training.fixed.length]
            } else {
                this.currentQuestion = this.questions[this.questions.length - 1]
            }
        } else {
            this.currentQuestion = this.questions[0]
        }

        return this.currentQuestion
    },

    makeNewTest(chatId) {
        this.testMode = true

        const user = userModule.getUser(chatId)
        const availableTest = user.training.seen

        let readyToTest = []
        let m = 6
        
        while (m--) {
            let ind = Math.floor(Math.random() * availableTest.length);
            
            if (!readyToTest.includes(availableTest[ind])) {
                readyToTest.push(availableTest[ind])
            } else {
                m++
            }
        }        
        
        readyToTest.map(id => {
            const Q = this.questions.find(question => question[0] == id)
            const QIndex = this.questions.indexOf(Q)

            let result = [this.questions[QIndex][2]]
            let n = 3
            
            while (n--) {
                let index = Math.floor(Math.random() * this.questions.length);

                if (!result.includes(this.questions[index][2])) {
                    result.push(this.questions[index][2])
                } else {
                    n++
                }
            }

            let randomResult = []
            let o = 4

            while (o--) {
                let randomIndex = Math.floor(Math.random() * 4);
                
                if (!randomResult.includes(randomIndex)) {
                    randomResult.push(randomIndex)
                } else {
                    o++
                }
            }
            
            const choices = [[result[randomResult[0]], result[randomResult[1]]], [result[randomResult[2]], result[randomResult[3]]]]

            const test =  {
                id,
                key: Q[1],
                answer: Q[2],
                choices
            }
            this.tests.push(test)
        })

        userModule.newRecord(chatId)
    },

    endTest() {
        this.testMode = false
        this.currentTestIndex = 0
        this.tests = []
    },

    setCurrentTestIndex(index) {
        this.currentTestIndex = index
    }
}

module.exports = questionsModule