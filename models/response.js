const questionsModule =  require("./questions");
const userModule =  require("./user");


const responseModule = {
    buttons: {
        start: "شروع آموزش",
        startTraining: "لغت جدید میخوام",
        startTesting: "میخوام امتحان بدم",
        next: "فهمیدم",
        enough: "خسته شدم"
    },
    
    messages: {
        welcome: "سلام، به بات آموزش زبان خوش آمدید\nبرای شروع کلیک کن",
        enough: "ایولا، خیلی خوب بود. وقت امتحانه",
        goTrainMore: "برای شروع امتحان حداقل ۶ کلمه باید دیده باشی!",
        correct: "✅",
        wrong: "❌",
        endTesting: "پایان"
    },

    start(chatId) {
        telegram.sendMessage(
            chatId,
            this.messages.welcome,
            {
                "reply_markup": {
                    "keyboard": [[this.buttons.start]]
                }
            }
        )
    },

    nextWord(chatId) {
        const word = questionsModule.makeNewWord(chatId)
        const response = `${ word[1] } \n ${ word[2] }`
        const keyboard = [[this.buttons.next], [this.buttons.enough]]

        telegram.sendMessage(
            chatId,
            response,
            {
                "reply_markup": {
                    "keyboard": keyboard
                }
            }
        )
    },

    enough(chatId) {
        const keyboard = [[this.buttons.startTraining], [this.buttons.startTesting]]

        telegram.sendMessage(
            chatId,
            this.messages.enough,
            {
                "reply_markup": {
                    "keyboard": keyboard
                }
            }
        )
    },

    goTrainMore(chatId) {
        const keyboard = [[this.buttons.startTraining]]

        telegram.sendMessage(
            chatId,
            this.messages.goTrainMore,
            {
                "reply_markup": {
                    "keyboard": keyboard
                }
            }
        )
    },

    showTest(chatId) {
        const testIndex = questionsModule.currentTestIndex
        const test = questionsModule.tests[testIndex]

        if (test) {
            telegram.sendMessage(
                chatId,
                test.key,
                {
                    "reply_markup": {
                        "keyboard": test.choices
                    }
                }
            )
        } else {
            const user = userModule.getUser(chatId)
            questionsModule.endTest()
            const estatics = user.history[user.history.length - 1]
            let corrects = 0
            estatics.map(item => {
                if (item == this.messages.correct) {
                    corrects++
                }
            })
            let message = this.messages.endTesting + "\n" + estatics.join('') + "\n" + (corrects/6*100).toFixed(1) + "%"
            
            const keyboard = [[this.buttons.startTraining], [this.buttons.startTesting]]

            telegram.sendMessage(
                chatId,
                message,
                {
                    "reply_markup": {
                        "keyboard": keyboard
                    }
                }
            )
        }
    },

    answer(chatId, answer) {
        const testIndex = questionsModule.currentTestIndex
        const test = questionsModule.tests[testIndex]
        const user = userModule.getUser(chatId)

        if (answer == test.answer) {
            telegram.sendMessage(
                chatId,
                this.messages.correct,
            )

            if (user.training.corrects.includes(test.id)) {
                userModule.removeWords(chatId, test.id, "corrects")
                userModule.removeWords(chatId, test.id, "seen")
                userModule.addWords(chatId, test.id, "fixed")
            } else if (user.training.seen.includes(test.id)) {
                userModule.addWords(chatId, test.id, "corrects")
            } else {
                userModule.removeWords(chatId, test.id, "wrongs")
                userModule.addWords(chatId, test.id, "corrects")
            }

            userModule.updateHistory(chatId, this.messages.correct)
        } else {
            telegram.sendMessage(
                chatId,
                this.messages.wrong,
            )

            if (user.training.corrects.includes(test.id)) {
                userModule.removeWords(chatId, test.id, "corrects")
                userModule.addWords(chatId, test.id, "wrongs")
            }

            if (user.training.seen.includes(test.id)) {
                userModule.addWords(chatId, test.id, "wrongs")
            }

            userModule.updateHistory(chatId, this.messages.wrong)
        }

        questionsModule.setCurrentTestIndex(testIndex + 1)
        this.showTest(chatId)
    }
}

module.exports = responseModule