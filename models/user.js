
const userModule = {
    users: [],

    addUser(newUser) {
        const user = this.users.find(user => user.id === newUser.id)
        if (!user) {
            this.users.push(
                { 
                    ...newUser,
                    training: { 
                        seen: [],
                        corrects: [],
                        wrongs: [],
                        termal: [],
                        fixed: []
                    },
                    history: []
                }
            )    
        }
    },
    
    getUser(id) {
        return this.users.find(user => user.id === id)
    },

    addWords(userId, WordId, target) {
        const index = this.users.findIndex(user => user.id === userId)
        if (index > -1) {
            if (!this.users[index].training[target].includes(WordId)) {
                this.users[index].training[target].push(WordId)
            }
        }
    },

    removeWords(userId, WordId, target) {
        const index = this.users.findIndex(user => user.id === userId)
        if (index > -1) {
            if (this.users[index].training[target].includes(WordId)) {
                const wordIndex = this.users[index].training[target].indexOf(WordId);
                this.users[index].training[target].splice(wordIndex, 1);
            }
        }
    },

    newRecord(userId) {
        const index = this.users.findIndex(user => user.id === userId)
        if (index > -1) {
            this.users[index].history.push([])
        }
    },

    updateHistory(userId, correctness) {
        const index = this.users.findIndex(user => user.id === userId)
        const historyTarget = this.users[index].history.length - 1
        if (index > -1 && historyTarget > -1) {
            this.users[index].history[historyTarget].push(correctness)
        }
    }
}

module.exports = userModule