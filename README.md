# Table of Contents

1. [Description](#description)
2. [Install the dependencies](#install-the-dependencies)
4. [Usage](#usage)


## Description

<center>Learning English Using Telegram Bot</center>

* client: [telegram bot](https://t.me/Zabaaaaanbot)
* server: nodeJs


## Install the dependencies

### Node installation
**You can find information about the installation on the [official Node.js website](https://nodejs.org/)**

### Git
**You can find git [here](https://git-scm.com/)**

If the installation was successful, you should be able to run the following command.

```bash
    $ node --version
    v8.11.3
```

```bash
    $ npm --version
    6.1.0
```

```bash
    $ git --version
    git version 2.23.0
```

## Usage

step-0: **Go to your workspace directory and run the commands**

step-1: **Clone the project and go to the directory**
```bash
    $ git clone https://gitlab.com/hesamr76/langbot.git LangBot
    $ cd LangBot
```
step-2: **Install requirements**
```bash
    $ npm install
```
step-3: **Start the server**
```bash
    $ node index
```
step-4: **join to the bot and press start (enjoy of learning 😍😇🔓)**

## Documentation
* [Telegram Api](https://core.telegram.org/bots/api)
* [NodeJS module Api](https://www.npmjs.org/package/node-telegram-bot-api)


## License
Copyright © 2020