const userModule =  require("./models/user");
const responseModule =  require("./models/response");
const questionsModule =  require("./models/questions");

const TelegramBot = require('node-telegram-bot-api');
const token = "1067599871:AAFV61D8IETf_dEVj3BNhB0hc5dlYvlnrYk"

telegram = new TelegramBot(token, { polling: true });

console.log("server is ok");
    
telegram.on('message', (msg) => {
    const chatId = msg.chat.id
    const text = msg.text    

    if (text === "/start") {
        responseModule.start(chatId)
        return   
    }
    
    if (questionsModule.testMode) {
        responseModule.answer(chatId, text)
        return
    }

    if (text.includes(responseModule.buttons.startTraining) || text.includes(responseModule.buttons.start)) {
        responseModule.nextWord(chatId)
    }

    if (text.includes(responseModule.buttons.next)) {
        telegram.deleteMessage(chatId,msg.message_id)
        userModule.addWords(chatId, questionsModule.currentQuestion[0], "seen")
        responseModule.nextWord(chatId)
    }

    if (text.includes(responseModule.buttons.enough)) {
        userModule.addWords(chatId, questionsModule.currentQuestion[0], "seen")
        responseModule.enough(chatId)
    }

    if (text.includes(responseModule.buttons.startTesting)) {
        if (userModule.getUser(chatId).training.seen.length > 5) {
            questionsModule.makeNewTest(chatId)
            responseModule.showTest(chatId)
        } else {
            responseModule.goTrainMore(chatId)
        }
    }
});

telegram.onText(/^\/start$/, (msg) => {
    userModule.addUser(msg.from)
});